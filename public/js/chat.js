const socket = io();

const getCookie = (name) => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
};

const appendToListRep = (message) => {
  $(`<li class='sent'><img src='public/image/message.jpg' alt='' /><p> ${message}
    </p></li>`).appendTo($('.messages ul'));
  $('.contact.active .preview').html(`<span>You: </span> ${message}`);
  $('.messages').animate({
    scrollTop: $(document).height(),
  }, 'fast');
};

const appendToList = (message) => {
  $(`<li class='replies'><img src='public/image/message.jpg' alt='' /><p> ${message}
    </p></li>`).appendTo($('.messages ul'));
  $('.contact.active .preview').html(`<span>You: </span> ${message}`);
  $('.messages').animate({
    scrollTop: $(document).height(),
  }, 'fast');
};

const newMessage = (userId) => {
  const userSend = {
    id: userId,
    message: $('.message-input input').val(),
  };
  if ($.trim(userSend.message) === '') {
    return;
  }
  $('.message-input input').val(null);
  appendToList(userSend.message);
  socket.emit('usersend', userSend);
};

$('.messages').animate({
  scrollTop: $(document).height(),
}, 'fast');


$('#profile-img').click(() => {
  $('#status-options').toggleClass('active');
});

$('.expand-button').click(() => {
  $('#profile').toggleClass('expanded');
  $('#contacts').toggleClass('expanded');
});

$('#status-options ul li').click(() => {
  $('#profile-img').removeClass();
  $('#status-online').removeClass('active');
  $('#status-away').removeClass('active');
  $('#status-busy').removeClass('active');
  $('#status-offline').removeClass('active');
  $(this).addClass('active');

  if ($('#status-online').hasClass('active')) {
    $('#profile-img').addClass('online');
  } else if ($('#status-away').hasClass('active')) {
    $('#profile-img').addClass('away');
  } else if ($('#status-busy').hasClass('active')) {
    $('#profile-img').addClass('busy');
  } else if ($('#status-offline').hasClass('active')) {
    $('#profile-img').addClass('offline');
  } else {
    $('#profile-img').removeClass();
  }

  $('#status-options').removeClass('active');
});


const showUser = (users) => {
  for (let i = 0; i < users.length; i += 1) {
    $(`<li class='contact' id='${users[i]._id}'><div class='wrap'><img src='public/image/user.jpg' alt='' />
    <div class='meta'><p class='name'>${users[i].username}
    </p><p class='preview'>...</p></div></div></li>`).appendTo($('#contacts ul'));
  }
};


let listUser;
let userId1;
$.ajax({
  type: 'GET',
  url: '/users',
  data: {
    get_param: 'value',
  },
  dataType: 'json',
  beforeSend: (xhr) => {
    /* Authorization header */
    xhr.setRequestHeader('Authorization', `Bearer ${getCookie('_token')}`);
  },
  success: (data) => {
    listUser = data;
    $('.content').css('display', 'none');
    showUser(listUser);
  },
}).done(() => {
  $('.contact').on('click', function () {
    $('.content').css('display', 'block');
    const userId = $(this).attr('id');
    // $.get('/message', (data1) => {
    //   for (let i = 0; i < data1.length; i += 1) {
    //     if (data1[i].usernames[0] === userId) {
    //       appendToList(data1[i].content);
    //     } else if (data1[i].usernames[1] === userId) {
    //       appendToListRep(data1[i].content);
    //     }
    //   }
    // });
    const found = listUser.find((element) => {
      return element._id === userId;
    });
    userId1 = found._id;
    document.getElementById('namere').innerHTML = found.username;
  });

  $(window).on('keydown', (e) => {
    if (e.which === 13) {
      newMessage(userId1);
    }
  });
  socket.on('serversend', (user) => {
    if ($.trim(user.message) === '') {
      return;
    }
    if (user.userId === userId1) {
      appendToListRep(user.message);
    }
  });
});
