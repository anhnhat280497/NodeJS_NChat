const path = require('path');
const fs = require('fs');

const srcDir = __dirname;
const root = path.dirname(srcDir);

const readFile = (Path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(Path, (err, data) => {
      return err ? reject(err) : resolve(data);
    });
  });
};

const writeFile = (fileName, content) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, content, (err) => {
      return err ? reject(err) : resolve(true);
    });
  });
};

module.exports = {
  readFile,
  rootPath: root,
  writeFile,
};
