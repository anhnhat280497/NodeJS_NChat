const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userRepo = require('../repository/user.mongo-repo');

class AuthService {
  constructor() {
    this.userRepo = userRepo;
  }

  async login(username, password) {
    // 1. tìm user
    const user = await this.userRepo.findByUsername(username);
    // 2. nếu có user kiểm tra password
    if (!user) {
      throw new Error('invalid username');
    }
    const isMatch = await bcrypt.compare(password, user.hashedPassword);
    if (isMatch) {
      delete user.hashedPassword;
      const token = jwt.sign({ username: user.username }, 'secret', { expiresIn: '7d' });
      return { user, token };
    }
    throw new Error('Invalid password');
  }
}

const authService = new AuthService();
module.exports = authService;
