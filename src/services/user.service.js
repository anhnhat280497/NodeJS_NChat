const bcrypt = require('bcrypt');
// const validator = require('validator');

const userRepo = require('../repository/user.mongo-repo');

class UsersServices {
  constructor() {
    this.userRepo = userRepo;
  }

  async findById(userId) {
    return this.userRepo.findById(userId);
  }

  async findByUsername(username) {
    return this.userRepo.findByUsername(username);
  }

  async createUser(userForm) {
    // validate form
    const error = {};
    const usernameRegex = /^[a-zA-Z0-9-.]+$/;
    if (!userForm.username || !usernameRegex.test(userForm.username)) {
      error['username'] = 'invalid username';
    }
    const userName = await this.userRepo.findByUsername(userForm.username);
    // console.log(userName);
    if (userName !== null) {
      if (userForm.username === userName.username) {
        error['username'] = 'invalid username';
      }
    }
    if (!userForm.password) {
      error['passwordd'] = 'invalid password';
    }

    if (userForm.password !== userForm.confirmpassword) {
      error['confirmpassword'] = 'confirmPassword mismatch';
    }

    if (Object.keys(error).length !== 0) {
      console.log(error);
      return error;
    }

    // hash password
    const hashedPassword = await bcrypt.hash(userForm.password, 10);
    delete userForm.password;
    delete userForm.confirmpassword;
    userForm.hashedPassword = hashedPassword;

    // store to database
    const newUser = await this.userRepo.createUser(userForm);
    delete newUser.hashedPassword;
    return newUser;
  }
}


const userService = new UsersServices();

module.exports = userService;
