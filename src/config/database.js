
const MongoClient = require('mongodb').MongoClient;

class MongoDbDatabase {
  constructor() {
    this.db = null;
    this.dbUrl = process.env.DATABASE_URL || 'mongodb://127.0.0.1:27017';
    this.databaseName = 'mongoNchat';
  }

  async connect() {
    const client = await MongoClient.connect(this.dbUrl, { useNewUrlParser: true });
    this.db = client.db(this.databaseName);
    return this.db;
  }

  getDb() {
    return this.db;
  }
}


const mongodb = new MongoDbDatabase();
module.exports = mongodb;
