const passport = require('passport');
const { Strategy } = require('passport-local');

const authService = require('./services/auth.service');
const userService = require('./services/user.service');

passport.use(new Strategy(async (username, password, done) => {
  try {
    const { user, token } = await authService.login(username, password);
    user.token = token;
    done(null, user);
  } catch (error) {
    done(error);
  }
}));

passport.serializeUser((user, callback) => callback(null, user._id));

passport.deserializeUser(async (id, callback) => {
  try {
    const user = await userService.findById(id);
    callback(null, user);
  } catch (error) {
    callback(error);
  }
});
