const { Router } = require('express');
const bcrypt = require('bcrypt');
const passport = require('passport');

const userRepo = require('../repository/user.mongo-repo');
const userService = require('../services/user.service');
const authMiddleware = require('./middleware/authRequiredMiddleware');

const loginFormController = (req, resp) => {
  resp.render('login', {
    title: 'Login',
  });
};

const registerFormController = (req, resp) => {
  resp.render('register', { register: 0 });
};

const createUserController = async (req, resp) => {
  try {
    const user = {
      username: req.body.username,
      password: req.body.password,
      confirmpassword: req.body.confirmpassword,
    };
    const userNew = await userService.createUser(user);
    const checkUser = await userRepo.findByUsername(userNew.username);
    if (checkUser !== null) {
      resp.redirect('/login');
    } else {
      resp.render('register', {
        title: 'Register',
      });
    }
  } catch (error) {
    resp.json({ error: error.message });
  }
};

const chatFormController = (req, resp) => {
  resp.render('chat', {
    title: 'NChat',
    user: req.user,
  });
};

const login1Controller = async (req, resp) => {
  try {
    const user = {
      username: req.body.username,
      password: req.body.password,
    };
    const checkUser = await userRepo.findByUsername(user.username);
    if (checkUser !== null) {
      const checkPass = await bcrypt.compare(user.password, checkUser.hashedPassword);
      if (checkPass) {
        console.log('Login OK!');
        resp.render('chat', {
          title: 'NChat',
          userValue: user,
        });
      } else {
        console.log('Incorrect Password!');
        resp.render('login', {
          title: 'Login',
          userValue: user,
        });
      }
    } else {
      console.log('Username not found!');
      resp.render('login', {
        title: 'Login',
      });
    }
  } catch (error) {
    resp.json({ error: error.message });
  }
};

const loginController = (req, resp) => {
  resp.cookie('_token', req.user.token, { maxAge: 7 * 24 * 60 * 60 * 1000 });
  resp.redirect('/chat');
};

const profileController = (req, resp) => {
  resp.render('profile', {
    user: req.user,
  });
};

const logoutController = (req, resp) => {
  req.logout();
  resp.redirect('/login');
};

const createAuthRoutes = () => {
  const authRoutes = Router();
  authRoutes.get('/login', loginFormController);
  authRoutes.post('/login', passport.authenticate('local', { failureRedirect: '/login' }), loginController);
  authRoutes.get('/register', registerFormController);
  authRoutes.post('/register', createUserController);
  authRoutes.get('/chat', authMiddleware, chatFormController);
  authRoutes.get('/profile', profileController);
  authRoutes.get('/logout', logoutController);
  return authRoutes;
};

const authRoutes = createAuthRoutes();

module.exports = authRoutes;
