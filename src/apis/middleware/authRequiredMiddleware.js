const isAuthenticated = (req, res, next) => {
  // do any checks you want to in here
  // Chưa đăng nhập thì về lại trang /login
  // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
  // you can do this however you want with whatever variables you set up
  if (req.isAuthenticated()) {
    return next();
  }

  // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
  return res.redirect('/login');
};

module.exports = isAuthenticated;
