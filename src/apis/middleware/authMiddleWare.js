const jwt = require('jsonwebtoken');
const userService = require('../../services/user.service');

const jwtVerify = token => new Promise((resolve, reject) => {
  jwt.verify(token, 'secret', (err, decoded) => {
    return err ? reject(err) : resolve(decoded);
  });
});

const authMiddleware = async (req, resp, next) => {
  // example: Bearer thisispassword
  const authvalue = req.headers.authorization;
  console.log(authvalue);
  if (!authvalue) {
    return resp.status(403).json({ error: 'invalid token' });
  }

  const tokens = authvalue.split(' ');
  if (tokens.length !== 2 || tokens[0] !== 'Bearer') {
    return resp.status(403).json({ error: 'invalid token' });
  }
  try {
    const payload = await jwtVerify(tokens[1]);
    const user = await userService.findByUsername(payload.username);
    req.user = user;
    console.log(payload);
    if (!user) {
      return resp.status(403).json({ error: 'invalid token' });
    }
    // console.log(payload);
    return next();
  } catch (error) {
    return resp.status(403).json({ error: 'invalid token' });
  }
};

module.exports = authMiddleware;
