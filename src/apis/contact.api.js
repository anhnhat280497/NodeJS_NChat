const { Router } = require('express');
const userRepo = require('../repository/user.mongo-repo');
const authMiddle = require('./middleware/authMiddleWare');

const allUsersController = async (req, resp) => {
  // req.user;
  const users = await userRepo.allUsersNotIn(req.user.username);
  resp.json(users);
};

const createContactRouters = () => {
  const contactRouter = Router();
  contactRouter.get('/users', authMiddle, allUsersController);
  return contactRouter;
};

const contactRouter = createContactRouters();

module.exports = contactRouter;
