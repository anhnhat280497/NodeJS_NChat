const { Router } = require('express');
const messRepo = require('../repository/message.mongo-repo');
const authMiddleware = require('./middleware/authRequiredMiddleware');

const allMessOfUsersController = async (req, resp) => {
  const mess = await messRepo.allMessageOfUsers(req.user._id);
  resp.json(mess);
};

const createMesstRouters = () => {
  const messageRouter = Router();
  messageRouter.get('/message', authMiddleware, allMessOfUsersController);
  return messageRouter;
};

const messageRouter = createMesstRouters();

module.exports = messageRouter;
