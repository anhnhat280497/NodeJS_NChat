const { ObjectId } = require('mongodb');

const mongodb = require('../config/database');

class UserMongoRepo {
  constructor() {
    this.mongodb = mongodb;
  }

  async allUsers() {
    const users = await this.userCollection().find({}).toArray();
    return users;
  }

  async allUsersNotIn(name) {
    const users = await this.userCollection().find({ username: { $nin: [name] } }).toArray();
    return users;
  }

  /**
   * Tim 1 user by id cua user do
   */
  async findById(userId) {
    if (!ObjectId.isValid(userId)) {
      throw new TypeError('Invalid userId');
    }
    // userId: string -> ObjecId
    const user = await this.userCollection().findOne({
      _id: ObjectId(userId),
    });
    return user;
  }

  async findByUsername(username) {
    const user = await this.userCollection().findOne({
      username,
    });
    return user;
  }

  async createUser(userForm) {
    const insertResult = await this.userCollection().insertOne(userForm);
    userForm.id = insertResult.insertedId;
    return userForm;
  }

  async deleteById(userId) {
    const deleteResult = await this.userCollection().deleteOne({
      _id: ObjectId(userId),
    });
    if (deleteResult.deletedCount === 1) {
      return true;
    }
    return false;
  }

  async updateById(userId, modifier) {
    const updatedResult = await this.userCollection().updateOne(
      { _id: ObjectId(userId) },
      { $set: modifier },
    );
    console.log(updatedResult);
    return true;
  }

  userCollection() {
    return this.mongodb.getDb().collection('user');
  }
}

const userRepo = new UserMongoRepo();

module.exports = userRepo;
