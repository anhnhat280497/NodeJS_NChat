const mongodb = require('../config/database');

class MessageMongoRepo {
  constructor() {
    this.mongodb = mongodb;
  }

  async allMessageOfUsers(userId) {
    const uId = `${userId}`;
    const messages = await this.messageCollection().find({
      usernames: uId,
    }).toArray();
    return messages;
  }

  async createMessage(user1, user2, message) {
    const insertResult = await this.messageCollection().insertOne({
      usernames: [user1, user2],
      content: message,
      createAt: new Date(),
    });
    return insertResult;
  }

  messageCollection() {
    return this.mongodb.getDb().collection('messages');
  }
}

const messageRepo = new MessageMongoRepo();

module.exports = messageRepo;
