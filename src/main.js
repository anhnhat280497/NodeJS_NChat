const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const bodyParser = require('body-parser');

// 1. import sesssion & passport
const session = require('express-session');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);

const mongodb = require('./config/database');
const utils = require('./utils');
const authRoutes = require('./apis/auth.api');
const contactRouters = require('./apis/contact.api');
const messageRouters = require('./apis/message.api');
const messRepo = require('./repository/message.mongo-repo');

// 3. require config
require('./passport');

const main = async () => {
  await mongodb.connect();
  const app = express();
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  // 2. config passport
  const sessionMiddleware = session({
    secret: 'aiashaksdjfh',
    resave: false,
    saveUninitialized: true,
    // h * min * s * 1000
    cookie: { maxAge: (7 * 24 * 60 * 60 * 1000) },
    store: new MongoStore({ db: mongodb.getDb() }),
  });
  app.use(sessionMiddleware);
  app.use(passport.initialize());
  app.use(passport.session());
  // done passport

  app.use('/public', express.static(path.join(utils.rootPath, 'public')));
  app.set('view engine', 'ejs');
  app.use('/', authRoutes);
  app.use('/', contactRouters);
  app.use('/', messageRouters);
  app.use('/', (res, resp) => resp.redirect('/chat'));

  const server = http.createServer(app);
  const io = socketio(server);

  io.use((socket, next) => {
    sessionMiddleware(socket.request, {}, next);
  });

  const connectingSocketUser = {};

  io.on('connection', (socket) => {
    const userId = socket.request.session.passport.user;
    const socketId = socket.id;
    connectingSocketUser[userId] = socketId;

    console.log('a user connected', socket.id, socket.request.session.passport.user);
    socket.on('disconnect', () => {
      console.log('user disconnect');
      delete connectingSocketUser[userId];
    });

    socket.on('usersend', (payload) => {
      const sendSocketId = connectingSocketUser[payload.id];

      if (sendSocketId) {
        io.to(sendSocketId).emit('serversend', { userId, message: payload.message });
        messRepo.createMessage(userId, payload.id, payload.message);
      }
    });
  });

  const port = +process.env.PORT || 6060;
  server.listen(port, '0.0.0.0', (err) => {
    if (err) {
      console.log('Create server got an error', err);
    } else {
      console.log('Server listening', port);
    }
  });
};

main();
